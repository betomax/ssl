﻿namespace Certificados
{
    partial class CreateCert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnSelectCA = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbKey = new System.Windows.Forms.Label();
            this.lbCA = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSelectKey = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtMac = new System.Windows.Forms.TextBox();
            this.btnSelectCNF = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.lbCNF = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtLocation = new System.Windows.Forms.TextBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.btnCreate = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtState = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtContry = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtPublicName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtArea = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtOrganization = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtSerie = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtClient = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtValidDays = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.lbScriptCMD = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnSelectCA
            // 
            this.btnSelectCA.Location = new System.Drawing.Point(6, 46);
            this.btnSelectCA.Name = "btnSelectCA";
            this.btnSelectCA.Size = new System.Drawing.Size(75, 23);
            this.btnSelectCA.TabIndex = 0;
            this.btnSelectCA.Text = "Certificado";
            this.btnSelectCA.UseVisualStyleBackColor = true;
            this.btnSelectCA.Click += new System.EventHandler(this.btnSelectCA_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(218, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Ubicación de llave privada  (.key)";
            // 
            // lbKey
            // 
            this.lbKey.AutoSize = true;
            this.lbKey.Location = new System.Drawing.Point(87, 108);
            this.lbKey.Name = "lbKey";
            this.lbKey.Size = new System.Drawing.Size(35, 13);
            this.lbKey.TabIndex = 2;
            this.lbKey.Text = "label2";
            // 
            // lbCA
            // 
            this.lbCA.AutoSize = true;
            this.lbCA.Location = new System.Drawing.Point(87, 56);
            this.lbCA.Name = "lbCA";
            this.lbCA.Size = new System.Drawing.Size(35, 13);
            this.lbCA.TabIndex = 4;
            this.lbCA.Text = "label3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(288, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Ubicación de certificado de autoridad (.pem)";
            // 
            // btnSelectKey
            // 
            this.btnSelectKey.Location = new System.Drawing.Point(6, 98);
            this.btnSelectKey.Name = "btnSelectKey";
            this.btnSelectKey.Size = new System.Drawing.Size(75, 23);
            this.btnSelectKey.TabIndex = 5;
            this.btnSelectKey.Text = "Llave";
            this.btnSelectKey.UseVisualStyleBackColor = true;
            this.btnSelectKey.Click += new System.EventHandler(this.btnSelectKey_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.txtMac);
            this.groupBox1.Controls.Add(this.btnSelectCNF);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.lbCNF);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtLocation);
            this.groupBox1.Controls.Add(this.txtID);
            this.groupBox1.Controls.Add(this.btnCreate);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtState);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtContry);
            this.groupBox1.Controls.Add(this.txtEmail);
            this.groupBox1.Controls.Add(this.txtPublicName);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtArea);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtOrganization);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(384, 72);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(354, 287);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos del cliente";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(230, 178);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(30, 13);
            this.label17.TabIndex = 54;
            this.label17.Text = "MAC";
            // 
            // txtMac
            // 
            this.txtMac.Location = new System.Drawing.Point(230, 194);
            this.txtMac.Name = "txtMac";
            this.txtMac.Size = new System.Drawing.Size(100, 20);
            this.txtMac.TabIndex = 53;
            // 
            // btnSelectCNF
            // 
            this.btnSelectCNF.Location = new System.Drawing.Point(6, 46);
            this.btnSelectCNF.Name = "btnSelectCNF";
            this.btnSelectCNF.Size = new System.Drawing.Size(90, 23);
            this.btnSelectCNF.TabIndex = 50;
            this.btnSelectCNF.Text = "Configuración";
            this.btnSelectCNF.UseVisualStyleBackColor = true;
            this.btnSelectCNF.Click += new System.EventHandler(this.btnSelectCNF_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(6, 26);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(286, 17);
            this.label16.TabIndex = 51;
            this.label16.Text = "Ubicación de archivo de configuración (.cnf)";
            // 
            // lbCNF
            // 
            this.lbCNF.AutoSize = true;
            this.lbCNF.Location = new System.Drawing.Point(102, 56);
            this.lbCNF.Name = "lbCNF";
            this.lbCNF.Size = new System.Drawing.Size(35, 13);
            this.lbCNF.TabIndex = 52;
            this.lbCNF.Text = "label3";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(230, 91);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(18, 13);
            this.label12.TabIndex = 49;
            this.label12.Text = "ID";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(230, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 34;
            this.label5.Text = "Ciudad";
            // 
            // txtLocation
            // 
            this.txtLocation.Location = new System.Drawing.Point(230, 150);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Size = new System.Drawing.Size(100, 20);
            this.txtLocation.TabIndex = 33;
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(230, 107);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(100, 20);
            this.txtID.TabIndex = 48;
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(273, 258);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(75, 23);
            this.btnCreate.TabIndex = 47;
            this.btnCreate.Text = "Crear";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(124, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 32;
            this.label3.Text = "Estado";
            // 
            // txtState
            // 
            this.txtState.Location = new System.Drawing.Point(124, 150);
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(100, 20);
            this.txtState.TabIndex = 31;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(124, 91);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 13);
            this.label9.TabIndex = 42;
            this.label9.Text = "Email";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 30;
            this.label2.Text = "Pais";
            // 
            // txtContry
            // 
            this.txtContry.Location = new System.Drawing.Point(17, 150);
            this.txtContry.Name = "txtContry";
            this.txtContry.Size = new System.Drawing.Size(100, 20);
            this.txtContry.TabIndex = 29;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(124, 107);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(100, 20);
            this.txtEmail.TabIndex = 41;
            // 
            // txtPublicName
            // 
            this.txtPublicName.Location = new System.Drawing.Point(18, 107);
            this.txtPublicName.Name = "txtPublicName";
            this.txtPublicName.Size = new System.Drawing.Size(100, 20);
            this.txtPublicName.TabIndex = 39;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 91);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 40;
            this.label8.Text = "URL o Nombre";
            // 
            // txtArea
            // 
            this.txtArea.Location = new System.Drawing.Point(124, 194);
            this.txtArea.Name = "txtArea";
            this.txtArea.Size = new System.Drawing.Size(100, 20);
            this.txtArea.TabIndex = 37;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(123, 178);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 13);
            this.label7.TabIndex = 38;
            this.label7.Text = "Area";
            // 
            // txtOrganization
            // 
            this.txtOrganization.Location = new System.Drawing.Point(17, 194);
            this.txtOrganization.Name = "txtOrganization";
            this.txtOrganization.Size = new System.Drawing.Size(100, 20);
            this.txtOrganization.TabIndex = 35;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 178);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 36;
            this.label6.Text = "Organizacion";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(191, 28);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(104, 13);
            this.label13.TabIndex = 51;
            this.label13.Text = "Contraseña del .PFX";
            // 
            // txtPass
            // 
            this.txtPass.Location = new System.Drawing.Point(191, 44);
            this.txtPass.Name = "txtPass";
            this.txtPass.Size = new System.Drawing.Size(100, 20);
            this.txtPass.TabIndex = 50;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(55, 83);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(84, 13);
            this.label11.TabIndex = 46;
            this.label11.Text = "Número de serie";
            // 
            // txtSerie
            // 
            this.txtSerie.Location = new System.Drawing.Point(55, 99);
            this.txtSerie.Name = "txtSerie";
            this.txtSerie.Size = new System.Drawing.Size(117, 20);
            this.txtSerie.TabIndex = 45;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(55, 28);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 13);
            this.label10.TabIndex = 44;
            this.label10.Text = "Nombre de Archivo";
            // 
            // txtClient
            // 
            this.txtClient.Location = new System.Drawing.Point(55, 44);
            this.txtClient.Name = "txtClient";
            this.txtClient.Size = new System.Drawing.Size(117, 20);
            this.txtClient.TabIndex = 43;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(191, 83);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 13);
            this.label14.TabIndex = 53;
            this.label14.Text = "Dias de vigencia";
            // 
            // txtValidDays
            // 
            this.txtValidDays.Location = new System.Drawing.Point(191, 99);
            this.txtValidDays.Name = "txtValidDays";
            this.txtValidDays.Size = new System.Drawing.Size(100, 20);
            this.txtValidDays.TabIndex = 52;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnSelectKey);
            this.groupBox2.Controls.Add(this.btnSelectCA);
            this.groupBox2.Controls.Add(this.lbKey);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.lbCA);
            this.groupBox2.Location = new System.Drawing.Point(24, 72);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(354, 144);
            this.groupBox2.TabIndex = 30;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Arhivos de autoridad certificadora";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(27, 29);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Origen";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(27, 9);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(306, 17);
            this.label15.TabIndex = 7;
            this.label15.Text = "Ubicación de archivo script de ejecución (.cmd)";
            // 
            // lbScriptCMD
            // 
            this.lbScriptCMD.AutoSize = true;
            this.lbScriptCMD.Location = new System.Drawing.Point(108, 39);
            this.lbScriptCMD.Name = "lbScriptCMD";
            this.lbScriptCMD.Size = new System.Drawing.Size(35, 13);
            this.lbScriptCMD.TabIndex = 8;
            this.lbScriptCMD.Text = "label3";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.txtValidDays);
            this.groupBox3.Controls.Add(this.txtSerie);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.txtClient);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.txtPass);
            this.groupBox3.Location = new System.Drawing.Point(27, 222);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(351, 138);
            this.groupBox3.TabIndex = 52;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Configuración del certificado";
            // 
            // CreateCert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 371);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.lbScriptCMD);
            this.Controls.Add(this.groupBox1);
            this.Name = "CreateCert";
            this.Text = "CreateCert";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnSelectCA;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbKey;
        private System.Windows.Forms.Label lbCA;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSelectKey;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtSerie;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtClient;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPublicName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtArea;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtOrganization;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtLocation;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtState;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtContry;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtValidDays;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lbScriptCMD;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnSelectCNF;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lbCNF;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtMac;
    }
}