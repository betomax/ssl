﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Certificados
{
    public partial class Demo : Form
    {
        private List<string> Errors = new List<string>();
        private X509Certificate2Collection certificates;
        private X509Certificate2 currentCert = null;

        public Demo()
        {
            InitializeComponent();
        }

        private void Demo_Load(object sender, EventArgs e)
        {
            getCertificates(StoreName.My, StoreLocation.CurrentUser);
        }

        private void getCertificates(StoreName storeName, StoreLocation storeLocation)
        {
            X509Store store = new X509Store(storeName, storeLocation);

            store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
            certificates = store.Certificates;

            List<ComboBoxItem> list = new List<ComboBoxItem>();
            foreach (var cert in certificates)
            {
                string cn = cert.IssuerName.Name.Split(',').First(s => s.Contains("CN=")).Replace("CN=", string.Empty);

                list.Add(new ComboBoxItem(cn, cert.SerialNumber));
            }
            comboBox1.DataSource = list;
            comboBox1.DisplayMember = "Text";
            comboBox1.ValueMember = "Value";
            store.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBoxItem select = (ComboBoxItem)comboBox1.SelectedItem;
            X509Certificate2Collection result = certificates.Find(X509FindType.FindBySerialNumber, select.Value, false);
            if (result.Count > 0)
            {
                currentCert = result[0];                
            }
        }

        private string Valid()
        {
            string flag = string.Empty;
            Errors.Clear();
            if (currentCert != null)
            {
                DateTime VF = currentCert.NotAfter;
                DateTime VT = currentCert.NotBefore;
                string SKI = null;

                if (DateTime.Compare(DateTime.Now, currentCert.NotAfter) > 0)
                {
                    Errors.Add("El certificado ya caduco: " + currentCert.NotAfter.ToLongDateString());
                }

                if (DateTime.Compare(DateTime.Now, currentCert.NotBefore) < 0)
                {
                    Errors.Add("El certificado no aplica: " + currentCert.NotAfter.ToLongDateString());
                }

                if (currentCert.GetNameInfo(X509NameType.SimpleName, true) != "www.precitool.com")
                {
                    Errors.Add("El certificado no pertenece a www.precitool.com");
                }

                foreach (var extension in currentCert.Extensions)
                {
                    if (extension.Oid.FriendlyName == "Subject Key Identifier" || extension.Oid.FriendlyName == "Identificador de clave del titular")
                    {
                        X509SubjectKeyIdentifierExtension ext = (X509SubjectKeyIdentifierExtension)extension;
                        string meMac = ext.SubjectKeyIdentifier.ToString();

                        try
                        {
                            var MacAddress = NetworkInterface
                                    .GetAllNetworkInterfaces()
                                    .Where(nic => nic.OperationalStatus == OperationalStatus.Up && nic.NetworkInterfaceType != NetworkInterfaceType.Loopback && nic.GetPhysicalAddress().ToString() == meMac)
                                    .Select(nic => nic.GetPhysicalAddress().ToString())
                                    .FirstOrDefault();
                            SKI = MacAddress;

                            if (MacAddress == null)
                            {
                                Errors.Add("El certificado no esta instalado en la maquina: " + meMac);
                            }
                        }
                        catch (Exception)
                        {
                            Errors.Add("El certificado no esta instalado en la maquina: " + meMac);
                        }
                    }
                }

                if (Errors.Count == 0)
                {
                    flag = string.Concat("VF=",VF.ToShortDateString(), ",VT=", VT.ToShortDateString(), ",SKI=", SKI);
                }
                else
                {
                    foreach (var error in Errors)
                    {
                        MessageBox.Show(error);
                    }
                }
            }
            else
            {
                Errors.Add("No ha seleccionado un certificado");
            }

            return flag;
        }

        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        private static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            string val = Valid();
            if (val != string.Empty)
            {
                if (txtPass.Text.Trim() != string.Empty && txtUser.Text.Trim() != string.Empty)
                {
                    HashAlgorithm algorithm = SHA256.Create();
                    byte[] bytes = algorithm.ComputeHash(Encoding.UTF8.GetBytes(txtPass.Text));
                    StringBuilder sb = new StringBuilder();
                    foreach (byte b in bytes)
                        sb.Append(b.ToString("X2"));
                    string pass = sb.ToString();

                    //MessageBox.Show("Hola " + txtUser.Text);
                    //MessageBox.Show(pass);
                    //string b64 = Base64Encode(val);
                    //MessageBox.Show(b64);
                    //MessageBox.Show(Base64Decode(b64));

                    SendData();
                }
            }
        }

        private void SendData()
        {
            string postData = "{\"Articulo\":\"\"}";
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create("https://demoesos.azure-api.net/v1/ArticulosWS");
            req.Method = "POST";
            req.ClientCertificates.Add(currentCert);
            byte[] data = Encoding.ASCII.GetBytes(postData);
            req.ContentLength = data.Length;
            req.ContentType = "application/json";
            req.Headers.Add("Ocp-Apim-Subscription-Key", "68df9e622db74953a7c79c1b4f6145ea");

            Stream dataStream = req.GetRequestStream();
            dataStream.Write(data, 0, data.Length);
            dataStream.Close();
            WebResponse response = req.GetResponse();
            Stream responseStream = response.GetResponseStream();

            response = req.GetResponse();
            StreamReader sr = new StreamReader(response.GetResponseStream());
            string result = sr.ReadToEnd();
            sr.Close();

            MessageBox.Show(result);
        }
    }
}
