﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Certificados
{
    public class CertDetails
    {
        public string Name { get; set; }
        public string HasPrivateKey { get; set; }
        public string Location { get; set; }
        public string Issuer { get; set; }
    }

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            getCertificates(StoreName.My, StoreLocation.CurrentUser);
        }

        private X509Certificate2 GetClientCertificate()
        {
            X509Store userCaStore = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            try
            {
                userCaStore.Open(OpenFlags.OpenExistingOnly);
                //Se obtienen solamente los certificados válidos en cuanto a fecha se refiera
                X509Certificate2Collection certificatesInStore = userCaStore.Certificates
                                                                .Find(X509FindType.FindByTimeValid, DateTime.Now, true);
                X509Certificate2 clientCertificate = null;
                if (certificatesInStore.Count > 0)
                {

                    clientCertificate = certificatesInStore[0];
                }
                else
                {
                    return null;
                }
                return clientCertificate;
            }
            catch
            {
                throw;
            }
            finally
            {
                userCaStore.Close();
            }
        }

        private void getCertificates(StoreName storeName, StoreLocation storeLocation)
        {
            X509Store store = new X509Store(storeName, storeLocation);

            store.Open(OpenFlags.ReadOnly);
            var certificates = store.Certificates;

            foreach (var cert in certificates)
            {
                string cn = cert.IssuerName.Name.Split(',').First(s => s.Contains("CN="));
                comboBox1.Items.Add(cn.Replace("CN=", string.Empty));
            }
            store.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }
    }
}
