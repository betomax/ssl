﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Certificados
{
    public partial class CertForm : Form
    {
        private X509Certificate2Collection certificates;
        private X509Certificate2 currentCert = null;

        public CertForm()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBoxItem select = (ComboBoxItem)comboBox1.SelectedItem;
            X509Certificate2Collection result = certificates.Find(X509FindType.FindBySerialNumber, select.Value, false);
            if (result.Count > 0)
            {
                try
                {
                    textBox2.Text = string.Empty;
                    X509ExtensionCollection extensions = result[0].Extensions;
                    foreach (X509Extension extension in result[0].Extensions)
                    {
                        textBox2.Text += string.Concat(extension.Oid.FriendlyName, "(", extension.Oid.Value, ")", Environment.NewLine);

                        if (extension.Oid.Value.ToString().Equals("1.2.3.45"))
                        {
                            AsnEncodedData asndata = new AsnEncodedData(extension.Oid, extension.RawData);
                            textBox2.Text += asndata.Format(true);
                        }

                        if (extension.Oid.FriendlyName == "Key Usage" || extension.Oid.FriendlyName == "Uso de la clave")
                        {
                            X509KeyUsageExtension ext = (X509KeyUsageExtension)extension;
                            //Console.WriteLine(ext.KeyUsages);
                            textBox2.Text += string.Concat(ext.KeyUsages.ToString(), Environment.NewLine);
                        }

                        if (extension.Oid.FriendlyName == "Basic Constraints" || extension.Oid.FriendlyName == "Restricciones básicas")
                        {
                            X509BasicConstraintsExtension ext = (X509BasicConstraintsExtension)extension;
                            //Console.WriteLine(ext.CertificateAuthority);
                            //Console.WriteLine(ext.HasPathLengthConstraint);
                            //Console.WriteLine(ext.PathLengthConstraint);
                            textBox2.Text += string.Concat(ext.CertificateAuthority.ToString(), Environment.NewLine);
                            textBox2.Text += string.Concat(ext.HasPathLengthConstraint.ToString(), Environment.NewLine);
                            textBox2.Text += string.Concat(ext.PathLengthConstraint.ToString(), Environment.NewLine);
                        }

                        if (extension.Oid.FriendlyName == "Subject Key Identifier" || extension.Oid.FriendlyName == "Identificador de clave del titular")
                        {
                            X509SubjectKeyIdentifierExtension ext = (X509SubjectKeyIdentifierExtension)extension;
                            //Console.WriteLine(ext.SubjectKeyIdentifier);
                            textBox2.Text += string.Concat(ext.SubjectKeyIdentifier.ToString(), Environment.NewLine);
                        }

                        if (extension.Oid.FriendlyName == "Enhanced Key Usage" || extension.Oid.FriendlyName == "Uso mejorado de claves")
                        {
                            X509EnhancedKeyUsageExtension ext = (X509EnhancedKeyUsageExtension)extension;
                            OidCollection oids = ext.EnhancedKeyUsages;
                            foreach (Oid oid in oids)
                            {
                                //Console.WriteLine(oid.FriendlyName + "(" + oid.Value + ")");
                                textBox2.Text += string.Concat(oid.FriendlyName, "(", oid.Value, ")", Environment.NewLine);
                            }
                        }

                        if (extension.Oid.FriendlyName == "Identificador de clave de entidad emisora")
                        {
                            AsnEncodedData asndata = new AsnEncodedData(extension.Oid, extension.RawData);
                            textBox2.Text += asndata.Format(true);
                        }
                    }
                }
                catch (Exception)
                {
                    
                }                

                currentCert = result[0];
                lbCert.Text = currentCert.IssuerName.Name.Split(',').First(s => s.Contains("CN=")).Replace("CN=", string.Empty); ;
                textBox1.Text = currentCert.IssuerName.Name;
                try
                {
                    byte[] rawdata = currentCert.RawData;
                    txtInfo.Text = string.Format("Content Type: {0}{1}", X509Certificate2.GetCertContentType(rawdata), Environment.NewLine);
                    txtInfo.Text += string.Format("Friendly Name: {0}{1}", currentCert.FriendlyName, Environment.NewLine);
                    txtInfo.Text += string.Format("Name: {0}{1}", arg0: currentCert.GetName(), arg1: Environment.NewLine);
                    txtInfo.Text += string.Format("Serie: {0}{1}", Convert.ToInt32(currentCert.SerialNumber, 16), Environment.NewLine);
                    txtInfo.Text += string.Format("Certificate Verified?: {0}{1}", currentCert.Verify(), Environment.NewLine);
                    txtInfo.Text += string.Format("Simple Name: {0}{1}", currentCert.GetNameInfo(X509NameType.SimpleName, true), Environment.NewLine);
                    txtInfo.Text += string.Format("Signature Algorithm: {0}{1}", currentCert.SignatureAlgorithm.FriendlyName, Environment.NewLine);
                    txtInfo.Text += string.Format("Private Key: {0}{1}", currentCert.PrivateKey.ToXmlString(false), Environment.NewLine);
                    txtInfo.Text += string.Format("Public Key: {0}{1}", currentCert.PublicKey.Key.ToXmlString(false), Environment.NewLine);
                    txtInfo.Text += string.Format("Certificate Archived?: {0}{1}", currentCert.Archived, Environment.NewLine);
                    txtInfo.Text += string.Format("Length of Raw Data: {0}{1}", currentCert.RawData.Length, Environment.NewLine);
                    txtInfo.Text += string.Format("Exp: {0}{1}", currentCert.NotAfter.ToString(), Environment.NewLine);
                    txtInfo.Text += string.Format("Valid: {0}{1}", currentCert.Verify(), Environment.NewLine);
                    txtInfo.Text += string.Format("Huella digital: {0}{1}", currentCert.Thumbprint, Environment.NewLine);
                    txtInfo.Text += string.Format("Subject: {0}{1}", currentCert.Subject, Environment.NewLine);
                    txtInfo.Text += string.Format("SubjectName: {0}{1}", currentCert.SubjectName.Name, Environment.NewLine);
                    txtInfo.Text += string.Format("Issuer: {0}{1}", currentCert.Issuer, Environment.NewLine);
                }
                catch (Exception)
                {
                    txtInfo.Text += string.Format("Public Key: {0}{1}", currentCert.PublicKey.Key.ToXmlString(false), Environment.NewLine);
                    txtInfo.Text += string.Format("Certificate Archived?: {0}{1}", currentCert.Archived, Environment.NewLine);
                    txtInfo.Text += string.Format("Length of Raw Data: {0}{1}", currentCert.RawData.Length, Environment.NewLine);
                    txtInfo.Text += string.Format("Exp: {0}{1}", currentCert.NotAfter.ToString(), Environment.NewLine);
                    txtInfo.Text += string.Format("Valid: {0}{1}", currentCert.Verify(), Environment.NewLine);
                    txtInfo.Text += string.Format("Huella digital: {0}{1}", currentCert.Thumbprint, Environment.NewLine);
                    txtInfo.Text += string.Format("Subject: {0}{1}", currentCert.Subject, Environment.NewLine);
                    txtInfo.Text += string.Format("SubjectName: {0}{1}", currentCert.SubjectName.Name, Environment.NewLine);
                    txtInfo.Text += string.Format("Issuer: {0}{1}", currentCert.Issuer, Environment.NewLine);
                }
            }
            else
            {
                lbCert.Text = string.Empty;
            }
        }

        private void CertForm_Load(object sender, EventArgs e)
        {
            getCertificates(StoreName.My, StoreLocation.CurrentUser);
        }

        private void getCertificates(StoreName storeName, StoreLocation storeLocation)
        {
            X509Store store = new X509Store(storeName, storeLocation);

            store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
            certificates = store.Certificates;

            List<ComboBoxItem> list = new List<ComboBoxItem>();
            foreach (var cert in certificates)
            {
                string cn = cert.IssuerName.Name.Split(',').First(s => s.Contains("CN=")).Replace("CN=", string.Empty);

                list.Add(new ComboBoxItem(cn, cert.SerialNumber));
            }
            comboBox1.DataSource = list;
            comboBox1.DisplayMember = "Text";
            comboBox1.ValueMember = "Value";
            store.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string yo = "9C8E99F35C18";
            var firstMacAddress = NetworkInterface
                                    .GetAllNetworkInterfaces()
                                    .Where(nic => nic.OperationalStatus == OperationalStatus.Up && nic.NetworkInterfaceType != NetworkInterfaceType.Loopback && nic.GetPhysicalAddress().ToString() == yo)
                                    .Select(nic => nic.GetPhysicalAddress().ToString())
                                    .FirstOrDefault();

            MessageBox.Show(firstMacAddress);
        }
    }
}
