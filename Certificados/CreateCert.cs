﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Certificados
{
    public partial class CreateCert : Form
    {
        string pathToFileCNF = Path.Combine(Environment.CurrentDirectory, "Cliente", "openssl.cnf");
        string pathToFileCMD = Path.Combine(Environment.CurrentDirectory, "Cliente", "client.cmd");
        List<string> fileCNF = new List<string>();
        List<string> fileCMD = new List<string>();
        public CreateCert()
        {
            InitializeComponent();
        }

        private void btnSelectCA_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                lbCA.Text = openFileDialog1.FileName;
            } 
        }

        private void btnSelectKey_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                lbKey.Text = openFileDialog1.FileName;
            }
        }

        private void LoadFileCNF(string pathCNF)
        {
            fileCNF.Clear();
            foreach (var line in File.ReadAllLines(pathCNF))
            {
                fileCNF.Add(line);
                txtContry.Text = line.Contains("countryName") ? line.Split('=')[1].Trim() : txtContry.Text;
                txtState.Text = line.Contains("stateOrProvinceName") ? line.Split('=')[1].Trim() : txtState.Text;
                txtLocation.Text = line.Contains("localityName") ? line.Split('=')[1].Trim() : txtLocation.Text;
                txtOrganization.Text = line.Contains("organizationName") ? line.Split('=')[1].Trim() : txtOrganization.Text;
                txtArea.Text = line.Contains("organizationalUnitName") ? line.Split('=')[1].Trim() : txtArea.Text;
                txtPublicName.Text = line.Contains("commonName") ? line.Split('=')[1].Trim() : txtPublicName.Text;
                txtEmail.Text = line.Contains("emailAddress") ? line.Split('=')[1].Trim() : txtEmail.Text;
                txtID.Text = line.Contains("subjectKeyIdentifier") ? line.Split('=')[1].Trim() : txtID.Text;
                txtMac.Text = line.Contains("MAC") ? line.Split('=')[1].Trim() : txtMac.Text;
            }
        }

        private void LoadFile(string pathCMD)
        {
            string key;
            string pem;
            foreach (var line in File.ReadAllLines(pathCMD.Replace('\\','/')))
            {
                if (line.Contains("set CONF"))
                {
                    string valCONF = line.Split('=')[1].Trim();
                    this.pathToFileCNF = valCONF.Remove(valCONF.Length - 1, 1).Remove(0, 1);
                    lbCNF.Text = this.pathToFileCNF;
                }
                else
                {
                    this.pathToFileCNF = lbCNF.Text;
                }

                if (line.Contains("set KEY"))
                {
                    string valCONF = line.Split('=')[1].Trim();
                    key = valCONF.Remove(valCONF.Length - 1, 1).Remove(0, 1);
                    lbKey.Text = key;
                }
                else
                {
                    lbKey.Text = string.Empty;
                }

                if (line.Contains("set PEM"))
                {
                    string valCONF = line.Split('=')[1].Trim();
                    pem = valCONF.Remove(valCONF.Length - 1, 1).Remove(0, 1);
                    lbCA.Text = pem;
                }
                else
                {
                    lbCA.Text = string.Empty;
                }
            }

            if (File.Exists(pathToFileCNF))
            {
                foreach (var line in File.ReadAllLines(pathToFileCNF))
                {
                    fileCNF.Add(line);
                    txtContry.Text = line.Contains("countryName") ? line.Split('=')[1].Trim() : txtContry.Text;
                    txtState.Text = line.Contains("stateOrProvinceName") ? line.Split('=')[1].Trim() : txtState.Text;
                    txtLocation.Text = line.Contains("localityName") ? line.Split('=')[1].Trim() : txtLocation.Text;
                    txtOrganization.Text = line.Contains("organizationName") ? line.Split('=')[1].Trim() : txtOrganization.Text;
                    txtArea.Text = line.Contains("organizationalUnitName") ? line.Split('=')[1].Trim() : txtArea.Text;
                    txtPublicName.Text = line.Contains("commonName") ? line.Split('=')[1].Trim() : txtPublicName.Text;
                    txtEmail.Text = line.Contains("emailAddress") ? line.Split('=')[1].Trim() : txtEmail.Text;
                    txtID.Text = line.Contains("subjectKeyIdentifier") ? line.Split('=')[1].Trim() : txtID.Text;
                }
            }

            if (File.Exists(pathCMD))
            {
                foreach (var line in File.ReadAllLines(pathCMD))
                {
                    fileCMD.Add(line);
                    lbKey.Text = line.Contains("set KEY") ? line.Split('=')[1].Trim() : lbKey.Text;
                    lbCA.Text = line.Contains("set PEM") ? line.Split('=')[1].Trim() : lbCA.Text;
                    txtClient.Text = line.Contains("set CLIENT_ID") ? line.Split('=')[1].Trim() : txtClient.Text;
                    txtSerie.Text = line.Contains("set CLIENT_SERIAL") ? line.Split('=')[1].Trim() : txtSerie.Text;
                    txtPass.Text = line.Contains("set PASS") ? line.Split('=')[1].Trim() : txtPass.Text;
                    txtValidDays.Text = line.Contains("set DAYS") ? line.Split('=')[1].Trim() : txtValidDays.Text;
                }
            }
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < fileCNF.Count; i++)
            {
                fileCNF[i] = fileCNF[i].Contains("countryName") ? string.Format("countryName             = {0}", txtContry.Text) : fileCNF[i];
                fileCNF[i] = fileCNF[i].Contains("stateOrProvinceName") ? string.Format("stateOrProvinceName     = {0}", txtState.Text) : fileCNF[i];
                fileCNF[i] = fileCNF[i].Contains("localityName") ? string.Format("localityName            = {0}", txtLocation.Text) : fileCNF[i];
                fileCNF[i] = fileCNF[i].Contains("organizationName") ? string.Format("organizationName        = {0}", txtOrganization.Text) : fileCNF[i];
                fileCNF[i] = fileCNF[i].Contains("organizationalUnitName") ? string.Format("organizationalUnitName  = {0}", txtArea.Text) : fileCNF[i];
                fileCNF[i] = fileCNF[i].Contains("commonName") ? string.Format("commonName              = {0}", txtPublicName.Text) : fileCNF[i];
                fileCNF[i] = fileCNF[i].Contains("emailAddress") ? string.Format("emailAddress            = {0}", txtEmail.Text) : fileCNF[i];
                fileCNF[i] = fileCNF[i].Contains("subjectKeyIdentifier") ? string.Format("subjectKeyIdentifier = {0}", txtID.Text) : fileCNF[i];

                if (fileCNF[i].Contains("MAC"))
                {
                    if (fileCNF[i] != "MAC = 1.2.3.45")
                    {
                        fileCNF[i] = string.Format("MAC                     = {0}", txtMac.Text);
                    }
                }
            }
            File.WriteAllLines(pathToFileCNF, fileCNF.ToArray());

            for (int i = 0; i < fileCMD.Count; i++)
            {
                fileCMD[i] = fileCMD[i].Contains("set CONF") ? string.Format(string.Concat("set CONF=", '"', "{0}", '"'), pathToFileCNF.Replace("\"","")) : fileCMD[i];
                fileCMD[i] = fileCMD[i].Contains("set KEY") ? string.Format(string.Concat("set KEY=", '"', "{0}", '"'), lbKey.Text.Replace("\"", "")) : fileCMD[i];
                fileCMD[i] = fileCMD[i].Contains("set PEM") ? string.Format(string.Concat("set PEM=", '"', "{0}", '"'), lbCA.Text.Replace("\"", "")) : fileCMD[i];
                fileCMD[i] = fileCMD[i].Contains("set CLIENT_ID") ? string.Format("set CLIENT_ID={0}", txtClient.Text) : fileCMD[i];
                fileCMD[i] = fileCMD[i].Contains("set CLIENT_SERIAL") ? string.Format("set CLIENT_SERIAL={0}", txtSerie.Text) : fileCMD[i];
                fileCMD[i] = fileCMD[i].Contains("set PASS") ? string.Format("set PASS={0}", txtPass.Text) : fileCMD[i];
                fileCMD[i] = fileCMD[i].Contains("set DAYS") ? string.Format("set DAYS={0}", txtValidDays.Text) : fileCMD[i];
            }
            File.WriteAllLines(pathToFileCMD, fileCMD.ToArray());

            ExecuteCommand("cd Cliente & client");

            try
            {
                using (SqlConnection conn = new SqlConnection("server=eosdb.centralus.cloudapp.azure.com;user id=Administrador; password= 3rco0roSuri@noz;initial catalog=demEos"))
                {
                    var cmd = new SqlCommand("insert into [demEos].[dbo].[MACAUTH] values (@mac)", conn);
                    cmd.Parameters.AddWithValue("@mac", txtMac.Text);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        void ExecuteCommand(string _Command)
        {
            //Indicamos que deseamos inicializar el proceso cmd.exe junto a un comando de arranque. 
            //(/C, le indicamos al proceso cmd que deseamos que cuando termine la tarea asignada se cierre el proceso).
            //Para mas informacion consulte la ayuda de la consola con cmd.exe /? 
            System.Diagnostics.ProcessStartInfo procStartInfo = new System.Diagnostics.ProcessStartInfo("cmd", "/c " + _Command);
            // Indicamos que la salida del proceso se redireccione en un Stream
            procStartInfo.RedirectStandardOutput = true;
            procStartInfo.UseShellExecute = false;
            //Indica que el proceso no despliegue una pantalla negra (El proceso se ejecuta en background)
            procStartInfo.CreateNoWindow = false;
            //Inicializa el proceso
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo = procStartInfo;
            proc.Start();
            //Consigue la salida de la Consola(Stream) y devuelve una cadena de texto
            string result = proc.StandardOutput.ReadToEnd();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                lbScriptCMD.Text = openFileDialog1.FileName;
                this.pathToFileCMD = lbScriptCMD.Text;
                this.LoadFile(this.pathToFileCMD);
            }
        }

        private void btnSelectCNF_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                lbCNF.Text = openFileDialog1.FileName;
                this.pathToFileCNF = lbCNF.Text;
                //Cargar datos del archivo
                this.LoadFileCNF(lbCNF.Text);
            }
        }
    }
}
